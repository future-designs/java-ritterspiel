/**
 * Class for actions like events, changes etc.
 * contain information about the changes and current gamestatus of the action
 */
public class Action
{
    private String actionString;

    /**
     * constructor
     */
    public Action()
    {
        actionString = null;
    }

    /**
     * setter for String actionString
     */
    protected void setActionString()
    {
        this.actionString = buildActionString();
        
        
    }

    /**
     * build function for actionString
     *
     * builds a String depending on the informations each action contains
     * used for output and logging
     * @return
     */
    protected String buildActionString()
    {
        return null;
    }

    /**
     * getter for actionString
     *
     * @return String
     */
    public String getActionString()
    {
        if(actionString != null) {
            return actionString;
        } else {
            setActionString();
            return actionString;
        }
    }

    /**
     * setter for PlayerSnapshot
     *
     * saves a static copy of a player and its current state of action
     */
    protected void setPlayerSnapshot()
    {
        
    }
}
