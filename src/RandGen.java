import java.util.Random;

/**
 * class for randomised int-based actions
 */
public class RandGen
{
    private Random randGen;

    /**
     * constructor
     */
    public RandGen()
    {
        randGen = new Random();
    }

    /**
     * getter for random int value
     * value depends on randRange parameter
     * value can be from 0 to randRange
     * @param int randRange - value for range
     * @return int
     */
    public int getRandomInt(int randRange)
    {
        return randGen.nextInt(randRange);
    }

    /**
     * getter for random boolean value
     * @return boolean
     */
    public boolean getRandomBool()
    {
        return randGen.nextBoolean();
    }

    /**
     * getter for random positive int
     * value depends on randRange parameter
     * value can be from 0 to randRange
     * @param int randRange - value for range
     * @return int
     */
    public int getRandomPositiveInt(int randRange)
    {
        int randInt = randGen.nextInt(randRange);
        if(randInt > 0) {
            return randInt;
        } else {
            return getRandomPositiveInt(randRange);
        }
    }
}
