import java.awt.event.*;
import javax.swing.Timer;

/**
 * class contains timer and sleeper to time events and programm actions
 */
public class Eventer implements ActionListener
{
    private int timerCount;
    private Timer timer;

    /**
     * constructor
     */
    public Eventer()
    {
        timer = new Timer(700, this);
        timerCount = 0;
    }

    /**
     * ActionListener method
     *
     * counts timer count up
     * @param e
     */
    public void actionPerformed(ActionEvent e)
    {
        timerCount++;
    }

    /**
     * getter for boolean timer value
     * @return boolean
     */
    public boolean timerRequest()
    {
        if(timerCount % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * sleeper function
     *
     * sleep a thread for specific timeperiod
     * @param int sleepTime
     */
    public void threadSleep(int sleepTime)
    {
        boolean sleeper = false;
        while(sleeper != true) {
            try {
                Thread.sleep(sleepTime);
                sleeper = true;
            } catch (InterruptedException e) {
                // We've been interrupted: no more messages.
                return;
            }
        }
    }

    /**
     * start timer
     */
    public void startStatusTimer()
    {
        timer.start();
    }

    /**
     * stop timer
     */
    public void stopStatusTimer()
    {
        timer.stop();
    }

    /**
     * setter for timer delay
     * @param int delay
     */
    public void setTimerDelay(int delay)
    {
        timer.setDelay(delay);
    }
    
}
