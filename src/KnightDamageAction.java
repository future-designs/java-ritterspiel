/**
 * class for damage actions relating to knight objects
 */
public class KnightDamageAction extends KnightAction
{
    // boolean value for option to output detailed informations
    private boolean detailed;

    /**
     * constructor
     * @param KnightList knightList
     * @param boolean detailed
     */
    public KnightDamageAction(KnightList knightList, boolean detailed)
    {
        super(knightList, 0);
        this.detailed = detailed;
        setNewLine(true);
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        if(getStaticPlayer() != null) {
            String actionString = "";
            actionString += getStaticPlayer().getIDPlayer(1).getName();
            actionString += " macht ";
            if(detailed == true) {
                actionString += getStaticPlayer().getIDPlayer(1).getFinalDamage();
            } else {
                actionString += getPlayer().getIDPlayer(1).getAllDamage();
                getPlayer().getIDPlayer(1).resetAllDamage();
            }
            actionString += " Schaden und bekommt von ";
            actionString += getStaticPlayer().getIDPlayer(2).getName();
            actionString += " ";
            if(detailed == true) {
                actionString += getStaticPlayer().getIDPlayer(2).getFinalDamage();
            } else {
                actionString += getPlayer().getIDPlayer(2).getAllDamage();
                getPlayer().getIDPlayer(2).resetAllDamage();
            }
            actionString += " Schaden.";
            return actionString;
        } else {
            System.out.println("Fehler: setDamageActionString in class(KnightDamageAction) getStaticPlayer() ist null.");
            return null;
        }
        
    }
}