import java.util.Iterator;

/**
 * class for simulating a single round (only one knight per duell left alive)
 */
public class KnightRoundSim extends RoundSim
{
    private KnightDuellList knightDuellList;
    private KnightActionList knightActionList;
    private int duellID;

    /**
     * constructor
     * @param list object knightActionList
     * @param boolean gui - option for GUI enable/disable
     * @param boolean detailed - option for detailed logging informations
     */
    public KnightRoundSim(KnightActionList knightActionList, boolean gui, boolean detailed)
    {
        super();
        this.knightActionList = knightActionList;
        duellID = 1;
    }

    /**
     * factory for one full performed RoundSim object
     * @param boolean gui - option for GUI enable/disable
     * @param boolean detailed - option for detailed logging informations
     * @return
     */
    public static KnightRoundSim factoryTest(boolean gui, boolean detailed)
    {
        KnightRoundSim knightRoundSim = new KnightRoundSim(new KnightActionList(), gui, detailed);
        
        knightRoundSim.prepareRound(KnightList.factoryTest(), detailed);
        knightRoundSim.performRound(gui, detailed);
        return knightRoundSim;
    }

    /**
     * function for preparing a round
     * initiates a knighDuellList with duellSims
     * @param list object knightList
     * @param boolean detailed - option for detailed logging informations
     */
    public void prepareRound(KnightList knightList, boolean detailed)
    {
        knightDuellList = KnightDuellList.factory(knightList, knightActionList, detailed);
    }

    /**
     * function for performing a single round
     * iterates over each DuellSim and calls theire main functions
     * @param boolean gui - option for GUI enable/disable
     * @param boolean detailed - option for detailed logging informations
     */
    public void performRound(boolean gui, boolean detailed)
    {
        Iterator<KnightDuellSim> it = knightDuellList.iterator();
        while(it.hasNext()) {
            KnightDuellSim knightDuellSim = it.next();
            knightDuellSim.simulate(detailed, getRoundID(), duellID); //boolean details
            duellID++;
        }
        plusRoundID();
    }

    /**
     * getter for all player(knights) from each DuellSim in KnightDuellList
     * iterates twice, once over knightDuellList to get duells, second over DuellSims to get player(knight) objects
     * @return list object knightList
     */
    public KnightList getPlayerList()
    {
        KnightList newKnightList = new KnightList();
        Iterator<KnightDuellSim> it = knightDuellList.iterator();
        while(it.hasNext()) {
            KnightDuellSim knightDuellSim = it.next();
            
            KnightList knightList = knightDuellSim.getPlayer();
            Iterator<Knight> knightIt = knightList.iterator();
            while(knightIt.hasNext()) {
                Knight knight = knightIt.next();
                newKnightList.addElement(knight);
            }
        }
        return newKnightList;
    }
}
