import java.util.Iterator;

/**
 * class for a List that should contains knight objects
 * the list can evaluate some attributs from the knight objects
 */
public class KnightActionList extends ActionList
{
    private KnightActionList[] duellActionList;
    private KnightActionList[] roundActionList;
    private int maxRound;
    private int maxDuell;

    /**
     * constructor
     */
    public KnightActionList()
    {
        super();
    }

    /**
     * setter for duellActionList
     * a list-array that is composed of lists that represents actions from different duells(ID)
     * iterates over the class object to build new lists depending on the attributes of included objects
     */
    public void setDuellActionList()
    {
        duellActionList = new KnightActionList[maxDuell];
        //System.out.println("MaxDuell: " + maxDuell);
        
        for(int i = 0; i < maxDuell; i++) {
            duellActionList[i] = new KnightActionList();
        }
        
        for(int i = 0; i < maxDuell; i++) {
            Iterator it = this.iterator();
            while(it.hasNext()) {
                KnightAction action = (KnightAction)it.next();
                if((action.getDuellID() - 1) == i) {
                    //System.out.println("Action-Duell-ID: " + action.getDuellID() + " i: " + i);
                    duellActionList[i].addElement(action);
                } else {
                    //System.out.println("Action-Duell-ID: " + action.getDuellID() + " i: " + i);
                }
            }
        }
    }

    /**
     * getter for duellActionList
     * @return KnightActionList duellActionList
     */
    public KnightActionList[] getDuellActionList()
    {
        return duellActionList;
    }

    /**
     * setter for roundActionList
     * a list-array that is composed of lists that represents actions from different rounds(ID)
     * iterates over the class object to build new lists depending on the attributes of included objects
     */
    public void setRoundActionList()
    {
        roundActionList = new KnightActionList[maxRound];
        
        for(int i = 0; i < maxRound; i++) {
            roundActionList[i] = new KnightActionList();
        }
        
        
        for(int i = 0; i < maxRound; i++) {
            Iterator it = this.iterator();
            while(it.hasNext()) {
                KnightAction action = (KnightAction)it.next();
                if((action.getRoundID() - 1) == i) {
                    roundActionList[i].addElement(action);
                }
            }
        }
    }

    /**
     * getter for roundActionList
     * @return KnightActionList roundActionList
     */
    public KnightActionList[] getRoundActionList()
    {
        return roundActionList;
    }

    /**
     * setter for maxDuell
     * iterates over inluded objects to get max duellID value
     */
    private void setMaxDuell()
    {
        Iterator it = this.iterator();
        while(it.hasNext()) {
            KnightAction action = (KnightAction)it.next();
            if(action.getDuellID() > maxDuell) {
                this.maxDuell = action.getDuellID();
            }
        }
    }
    /**
     * setter for maxRound
     * iterates over inluded objects to get max roundID value
     */
    private void setMaxRound()
    {
        Iterator it = this.iterator();
        while(it.hasNext()) {
            KnightAction action = (KnightAction)it.next();
            if(action.getRoundID() > maxRound) {
                this.maxRound = action.getRoundID();
            }
        }
    }

    /**
     * function for updating the list
     * sets duellID and roundID depending on new object-informations
     * recreates duell- and roundActionLists
     */
    public void updateActionList()
    {
        setMaxDuell();
        setMaxRound();
        setRoundActionList();
        setDuellActionList();
    }
}