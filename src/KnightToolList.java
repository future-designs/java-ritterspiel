import java.util.Iterator;

/**
 * class for a list of KnightTools
 */
public class KnightToolList extends ToolList
{
    private RandGen randGen;
    /**
     * constructor
     */
    public KnightToolList()
    {
       randGen = new RandGen();
    }

    /**
     * factory for KnightToolList
     * builds new toolList with predefined tools
     * @return list object toolList
     */
    public static KnightToolList factoryTools()
    {
        KnightToolList toolList = new KnightToolList();
        toolList.addElement(new Staff());
        toolList.addElement(new Shield());
        toolList.addElement(new MagicStaff());
        toolList.addElement(new HealPotion());
        toolList.addElement(new MagicMorphTool());
        return toolList;
    }

    /**
     * getter for random object from list
     * selects a random tool from list with a predefined chance
     * if no tools left or a tool is not selected dummy-tools are returned
     * @return object tool
     */
    public KnightTool getRandomObject()
    {
        randGen = new RandGen();
        KnightTool tempObject = null;
        
        Iterator<KnightTool> it = this.iterator();
        if(it.hasNext()) {
            for(int i = 0; i <= randGen.getRandomInt(this.getObjectCount()); i++) {
                tempObject = it.next();
            }
                
            // random tool controlling
            if(tempObject.toolControl()) {
                it.remove();
                return tempObject;
            } else {
                // debug
                //System.out.println("Dummy Tool");
                
                return getDummyTool();
            }
        } else {

            // debug
            //System.out.println("Dummy Tool");

            return getDummyTool();
        }
        
    }

    /**
     * getter for dummy-tool
     * creates new dummy-tool
     * @return object tool
     */
    public KnightTool getDummyTool()
    {
        KnightTool dummyTool = new DummyTool();
        return dummyTool;
    }
    
    /**
     * factory method for CustomList
     * used for getting the right child type of object in parent copy method
     * @return Object KnightToolList
     */
    public KnightToolList factory()
    {
        KnightToolList newList = new KnightToolList();
        
        Iterator<KnightTool> it = this.iterator();
        while(it.hasNext()) {
            KnightTool tool = (KnightTool)it.next().copy();
            newList.addElement(tool);
        }
        
        return newList;
    }
}
