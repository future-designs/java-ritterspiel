import java.io.*;

/**
 * class for knight, a player with special attributs
 */
public class Knight extends Player implements Serializable
{
    private RandGen randGen;
    private KnightToolList toolList;
    private KnightTool activeTool;

    // boolean value represent status alive or dead of knight
    private boolean alive;
    // int value for last calculated random damage
    private int randDamage;
    // int value for current calculated final damage (consider tool, crit etc.)
    private int finalDamage;
    // int value for all damage dealed at obeject-lifetime (or until resettet)
    private int allDamage;

    private final int BASEDAMAGE = 10;
    private int duellID;
    private int roundID;
    private String tempName;

    /**
     * constructor
     * @param String name - name attribut of the knight object
     */
    public Knight(String name)
    {
        super(name);
        this.alive = true;
        this.activeTool = null;
        this.finalDamage = 0;
        this.duellID = 0;
        this.allDamage = 0;
        this.roundID = 0;
        this.duellID = 0;
        this.randDamage = 0;
        this.tempName = name;
        randGen = new RandGen();
        toolList = KnightToolList.factoryTools();
    }

    /**
     * getter for alive variable
     * @return boolean
     */
    public boolean getAlive()
    {
        return alive;
    }

    /**
     * checks points and sets alive variable
     * depends on left points of the knight object
     */
    public void checkAlive()
    {
        if(this.getPoints() <= 0) {
            alive = false;
        }
    }
    
    /**
     * setter for alive variable
     * @param boolean
     */
    public void setAlive(boolean alive)
    {
        this.alive = alive;
    }

    /**
     * function for getting random damage value
     * ATTENTION - sets randDamage vaiable as well and is getter for it
     * uses Rand for calculating int value
     * @return int
     */
    public int getDamage()
    {
        this.randDamage = randGen.getRandomPositiveInt(BASEDAMAGE);
        return randDamage;
    }

    /**
     * getter for current randDamage value
     * @return int
     */
    public int getRandDamage()
    {
        return randDamage;
    }
    
    /**
     * setter for randRamage
     * @param int
     */
    public void setRandDamage(int randDamage)
    {
        this.randDamage = randDamage;
    }
    
    /**
     * getter for random object from knightToolList
     * ATTENTION - sets activeTool as well
     * @return object - tool object
     */
    public KnightTool getRandomObject()
    {
        KnightTool tool = toolList.getRandomObject();
        this.activeTool = tool;
        return tool;
    }

    /**
     * function for resetting the toollist
     * calls factory method of toolList to recreate tools
     */
    public void resetToolList()
    {
        this.toolList = KnightToolList.factoryTools();
    }

    /**
     * manually setter for activeTool
     * ATTENTION - method getRandomObject() sets the variable as well
     */
    public void randomTool(KnightActionList knightActionList)
    {
        this.activeTool = toolList.getRandomObject();
        
        if(activeTool == null) {
            System.out.println("Fehler: randomTool() in class(Knight) activeTool ist null.");
        }
    }

    /**
     * getter for object referenced to variable activeTool
     * @return object - tool object
     */
    public KnightTool getActiveTool()
    {
        return activeTool;
    }
    
    /**
     * setter for activeTool
     * @param Object KnightTool
     */
    
    public void setActiveTool(KnightTool tool)
    {
        this.activeTool = tool;
    }

    /**
     * setter for variable finalDamage
     * ATTENTION - adds parameter value to allDamage variable
     * @param int finalDamage
     */
    public void addFinalDamage(int finalDamage)
    {
        this.allDamage += finalDamage;
        this.finalDamage = finalDamage;
    }

    /**
     * setter for variable finalDamage
     * @param int finalDamage
     */
    public void setFinalDamage(int finalDamage)
    {
        this.finalDamage = finalDamage;
    }
    
    /**
     * getter for finalDamage variable
     * @return int finalDamage
     */
    public int getFinalDamage()
    {
        return finalDamage;
    }

    /**
     * setter for allDamage variable
     * @param int allDamage
     */
    public void setAllDamage(int allDamage)
    {
        this.allDamage = allDamage;
    }

    /**
     * getter for allDamage variable
     * @return int allDamage
     */
    public int getAllDamage()
    {
        return allDamage;
    }

    /**
     * function for resetting allDamage variable
     * sets value to 0
     */
    public void resetAllDamage()
    {
        this.allDamage = 0;
    }

    /**
     * getter for roundID variable
     * @return int roundID
     */
    public int getRoundID()
    {
        return roundID;
    }

    /**
     * setter for roundID variable
     * @param int roundID
     */
    public void setRoundID(int roundID)
    {
        this.roundID = roundID;
    }

    /**
     * setter for duellID variable
     * @param int duellID
     */
    public void setDuellID(int duellID)
    {
        this.duellID = duellID;
    }

    /**
     * getter for duellID variable
     * @return int duellID
     */
    public int getDuellID()
    {
        return duellID;
    }
    
    /**
     * getter for KnightToolList
     * @return Object KnightToolList
     */
    
    public KnightToolList getKnightToolList()
    {
        return toolList;
    }
    
    /**
     * setter for KnightToolList
     * @param Object KnightToolList
     */
    public void setKnightToolList(KnightToolList toolList)
    {
        this.toolList = toolList;
    }
    
    /**
     * factory method
     * @return Object Knight
     */
    public Knight factory()
    {
        Knight newKnight = new Knight(this.getClearName());

        newKnight.setKnightToolList((KnightToolList)this.getKnightToolList().copy());
        if(this.getActiveTool() == null) {
            // debug
            //System.out.println("ActiveTool ist null.");
        } else {
            newKnight.setActiveTool((KnightTool)this.getActiveTool().copy());
        }
        
        newKnight.setAlive(this.getAlive());
        newKnight.setRandDamage(this.getRandDamage());
        newKnight.setFinalDamage(this.getFinalDamage());
        newKnight.setAllDamage(this.getAllDamage());
        newKnight.setDuellID(this.getDuellID());
        newKnight.setRoundID(this.getRoundID());
        
        return newKnight;
    }
    
    /**
     * tempName setter
     * @param String
     */
    public void setTempName(String tempName)
    {
        this.tempName = tempName;
    }
    
    /**
     * tempName getter
     * @return String
     */
    public String getTempName()
    {
        return tempName;
    }
}
