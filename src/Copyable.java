public interface Copyable
{
    /**
     * factory method usage for copy method of parent
     * @return Object
     */
    public Object factory();
}
