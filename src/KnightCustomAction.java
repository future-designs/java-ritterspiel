/**
 * class for custom action
 * contains custom string to build custom string
 * can be used for various situations
 */
public class KnightCustomAction extends KnightAction
{
    private String customString;

    /**
     * constructor
     * @param KnigthList knightList - list of knight objects
     * @param String customString - string for customation of the action
     * @param int playerID
     */
    public KnightCustomAction(KnightList knightList, String customString, int playerID)
    {
        super(knightList, playerID);
        this.customString = customString;
        setActionString();
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        if(getStaticPlayer() != null) {
            String actionString = "";
            actionString += getStaticPlayer().getIDPlayer(getPlayerID()).getName();
            actionString += " ";
            actionString += customString;
            return actionString;
        } else {
            System.out.println("Fehler: setCustomActionString in class(CustomAction) staticPlayer ist null.");
            return null;
        }
    }
}