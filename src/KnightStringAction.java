/**
 * class for simply string action
 * can be used for various situations
 */
public class KnightStringAction extends KnightAction
{
    private String customString;
    /**
     * constructor
     */
    public KnightStringAction(KnightList knightList, String customString)
    {
        super(knightList, 0);
        this.customString = customString;
        setActionString();
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString(String customString)
    {
            String actionString = customString;
            return actionString;
    }
    
    
}