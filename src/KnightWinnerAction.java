/**
 * class for winner actions (who has won the duell)
 */
public class KnightWinnerAction extends KnightAction
{
    /**
     * constructor
     * @param list object knightList
     */
    public KnightWinnerAction(KnightList knightList)
    {
        super(knightList, 0);
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        String actionString = "";
        if(getStaticPlayer().getIDPlayer(1).getAlive() == true && getStaticPlayer().getIDPlayer(2).getAlive() == false) {
            actionString += getStaticPlayer().getIDPlayer(1).getName();
            actionString += " besiegt ";
            actionString += getStaticPlayer().getIDPlayer(2).getName();
            actionString += ".";
            return actionString;
        } else if(getStaticPlayer().getIDPlayer(2).getAlive() == true && getStaticPlayer().getIDPlayer(1).getAlive() == false) {
            actionString += getStaticPlayer().getIDPlayer(2).getName();
            actionString += " besiegt ";
            actionString += getStaticPlayer().getIDPlayer(1).getName();
            actionString += ".";
            return actionString;
        } else {
            System.out.println("Fehler ! Beide Spieler leben oder sind tot.");
            return null;
        }
    }
}
