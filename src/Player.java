/**
 * class for player with specific attributs
 */
public class Player implements Copyable
{
    private String name;
    private int playerID;
    // value for current points
    private int points;
    // value for max reachable points
    private final int MAXPOINTS;

    /**
     * constructor
     * @param String name
     */
    public Player(String name)
    {
        this.name = name;
        this.MAXPOINTS = 100;
        this.points = MAXPOINTS;
        this.playerID = 0;
    }

    /**
     * setter for variable name
     * @param String name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * getter for variable name with points value
     * @return String name
     */
    public String getName()
    {
        String newName;
        newName = name;
        newName += "(";
        newName += points;
        newName += ")";
        
        return newName;
    }

    /**
     * getter for variable name without any additional informations
     * @return String name
     */
    public String getClearName()
    {
        return name;
    }
    
    /**
     * function to reset points
     * sets variable points to MAXPOINTS
     */
    public void resetPoints()
    {
        this.setPoints(MAXPOINTS);
    }

    /**
     * function to decrease points
     * decreases points if not less zero
     * if points less zero set points zero
     * @param int points
     */
    protected void decreasePoints(int points)
    {
        int tempPoints = getPoints();
        tempPoints -= points;
        if(tempPoints < 0) {
             setPoints(0);
        } else {
            setPoints(tempPoints);
        }
    }

    /**
     * function to increase points
     * increase points if not more than MAXPOINTS
     * if points more than MAXPOINTS set points to MAXPOINTS
     * @param points
     */
    protected void increasePoints(int points)
    {
        int tempPoints = getPoints();
        tempPoints += points;
        if(tempPoints > MAXPOINTS) {
             setPoints(MAXPOINTS);
        } else {
            setPoints(tempPoints);
        }
    }

    /**
     * setter for variable playerID
     * @param int playerID
     */
    public void setPlayerID(int playerID)
    {
        this.playerID = playerID;
    }

    /**
     * getter for variable playerID
     * @return id
     */
    public int getPlayerID()
    {
        return playerID;
    }

    /**
     * setter for variable points
     * @param int points
     */
    protected void setPoints(int points)
    {
        this.points = points;
    }

    /**
     * getter for variable points
     * @return int
     */
    protected int getPoints()
    {
        return points;
    }
    
    /**
     * copy method to get a new object with same attributs
     * @return object Player
     */
    public Player copy()
    {
        Player newPlayer = factory();
        newPlayer.setPoints(this.getPoints());
        newPlayer.setPlayerID(this.getPlayerID());
        return newPlayer;
    }
    
    /**
     * factory method
     * @return object Player
     */
    public Player factory()
    {
        return new Player(this.getClearName());
    }
}
