/**
 * class for tool objects
 * tools are used by players and can have various attributs
 */
public class Tool implements Copyable
{
    private String name;
    private String description;

    /**
     * constructor
     */
    public Tool()
    {
        this.name = null;
        this.description = null;
    }

    /**
     * setter for variable name
     * @param String name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * getter for variable name
     * @return String
     */
    public String getName()
    {
        return name;
    }

    /**
     * setter for variable description
     * @param String description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * getter for variable description
     * @return String
     */
    public String getDescription()
    {
        return this.description;
    }
    
    /**
     * copy method to get a new object with same attributs
     * @return object Tool
     */
    public Tool copy()
    {
        if(this != null) {
            Tool newTool = factory();
            newTool.setName(this.getName());
            newTool.setDescription(this.getDescription());
            return newTool;
        } else {
            return null;
        }
    }
    
    /**
     * factory method
     * @return object KnightTool
     */
    public Tool factory()
    {
        Tool newTool = new Tool();
        return newTool;
    }
}