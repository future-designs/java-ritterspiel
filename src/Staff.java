/**
 * class for specific tool staff
 */
public class Staff extends KnightTool
{
    /**
     * constructor
     */
    public Staff()
    {
       super();
       setCritMult(2);
       setName("Staff");
       setDescription("verdoppelt den eigenen Schaden");
    }
    
    /**
     * factory method
     * @return object Staff
     */
    public Staff factory()
    {
        return new Staff();
    }
}
