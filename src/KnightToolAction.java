/**
 * class for tool actions
 */
public class KnightToolAction extends KnightAction
{
    /**
     * constructor
     * @param list object knightList
     * @param int playerID
     */
    public KnightToolAction(KnightList knightList, int playerID)
    {
        super(knightList, playerID);
        
        if(!knightList.getIDPlayer(playerID).getActiveTool().getVisible()) {
            setVisible(false);
        }
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        if(getStaticPlayer() != null) {
            String actionString = "";
            KnightTool tool = (KnightTool)getStaticPlayer().getIDPlayer(getPlayerID()).getActiveTool();
            if(tool.getDescription() != null) {
                actionString = getStaticPlayer().getIDPlayer(getPlayerID()).getName();
                actionString += " nutzt ";
                actionString += tool.getName();
                actionString += " ";
                actionString += tool.getDescription();
            } else {
                actionString = getStaticPlayer().getIDPlayer(getPlayerID()).getName();
                actionString += " nutzt kein Tool";
            }
            return actionString;
        } else {
            System.out.println("Fehler: setToolActionString in class(ToolAction) playerSnapshotList ist null.");
            return null;
        }
    }
}
