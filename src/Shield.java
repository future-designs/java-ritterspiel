/**
 * class for specific tool shield
 */
public class Shield extends KnightTool
{
    /**
     * consructor
     */
    public Shield()
    {
        super();
        setCritMult(-1);
        setName("Shield");
        setDescription("reflektiert gesamten eingehenden Schaden");
    }
    
    /**
     * factory method
     * @return object Shield
     */
    public Shield factory()
    {
        return new Shield();
    }
}