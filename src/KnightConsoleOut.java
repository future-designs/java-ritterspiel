import java.util.Iterator;

/**
 * Class for all Console Output and logging
 */
public class KnightConsoleOut extends ConsoleOut
{
    private Eventer eventer;
    
    /**
     * constructor
     */
    public KnightConsoleOut()
    {
        super();
        eventer = new Eventer();
    }
    
    /**
     * class for printing out duell-winner and  competition winner
     * iterates over knightActionList to get actions with one alive player
     * prints out a custom string build from the action informations
     */
    public void outputWinner(KnightActionList knightActionList)
    {
        System.out.println();
        System.out.println("Die Gewinner der Duelle sind:");
        System.out.println();
        eventer.threadSleep(500);
        
        Iterator knightIt = knightActionList.iterator();
        int winnerID = 0;
        KnightAction tempAction = null;
        while(knightIt.hasNext()) {
            KnightAction action = (KnightAction)knightIt.next();
            if(action.getStaticPlayer().getIDPlayer(1).getAlive() == true && action.getStaticPlayer().getIDPlayer(2).getAlive() == false) {
                System.out.println("Duell " + action.getDuellID() + ": " + action.getStaticPlayer().getIDPlayer(1).getClearName() + " (Er besiegte " + action.getStaticPlayer().getIDPlayer(2).getClearName() + ")");
                winnerID = 1;
                eventer.threadSleep(500);
            } else if (action.getStaticPlayer().getIDPlayer(1).getAlive() == false && action.getStaticPlayer().getIDPlayer(2).getAlive() == true) {
                System.out.println("Duell " + action.getDuellID() + ": " + action.getStaticPlayer().getIDPlayer(2).getClearName() + " (Er besiegte " + action.getStaticPlayer().getIDPlayer(1).getClearName() + ")");
                winnerID = 2;
                eventer.threadSleep(500);
            }
            tempAction = action;
        }
        
        System.out.println();
        System.out.println("Der Sieger des Tuniers ist: " + tempAction.getPlayer().getIDPlayer(winnerID).getClearName());
        System.out.println();
    }
    
    /**
     * class for printing out all actions of a single round (duell)
     * iterates over knightActionList and prints out every actionString
     * uses parser to get user-input
     */
    public void outputAllRounds(KnightActionList knightActionList, KnightInputParser parser)
    {
        knightActionList.updateActionList();
        KnightActionList[] knightRoundList = knightActionList.getRoundActionList();
        
        for(int i = 0; i < knightRoundList.length; i++) {
            Iterator it = knightRoundList[i].iterator();
            if(i == 0) {
                parser.getStringCommand("Wollen Sie das Tunier starten ?");
            } else {
                parser.getStringCommand("Wollen Sie die nächste Runde starten ?");
            }
            while(it.hasNext()) {
                KnightAction knightAction = (KnightAction)it.next();
                
                // visibility of action
                while(!knightAction.getVisible()) {
                knightAction = (KnightAction)it.next();
                }
                
                String actionString =  knightAction.getActionString();
                
                // debug
                //System.out.println("RoundID: " + knightAction.getRoundID() + ", DuellID: " + knightAction.getDuellID() + " ,"+ actionString);
                
                System.out.println("Duell: " + knightAction.getDuellID() + " ,"+ actionString);
                
                // line break of action
                if(knightAction.getNewLine()) {
                    System.out.println();
                }
                
                eventer.threadSleep(400);
            }
        }
    }
}
