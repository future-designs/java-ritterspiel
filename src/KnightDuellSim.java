import java.util.Iterator;

/**
 * class for simulating a duell with knight objects
 */
public class KnightDuellSim extends DuellSim
{
    private KnightList knightList = new KnightList();
    private KnightActionList knightActionList;
    private KnightDamageCalculator damageCalc;

    /**
     * constructor
     * sets playerIDs for every knight object in knightList
     * @param list object knightList
     * @param list object knightActionList
     * @param boolean detailed - value for option to log detailed informations
     */
    public KnightDuellSim(KnightList knightList, KnightActionList knightActionList, boolean detailed)
    {
        super();
        Iterator it = knightList.iterator();
        int i = 1;
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            knight.setPlayerID(i);
            this.knightList.addElement(knight);
            i++;
        }
        
        this.knightActionList = knightActionList;
        damageCalc = new KnightDamageCalculator(knightActionList);
    }

    /**
     * getter for knightList
     * @return list object knightList
     */
    public KnightList getPlayer()
    {
        return knightList;
    }

    /**
     * function for simulating one duell
     * sets roundID and duellID (calls setID())
     * calls damageCalculator configuration and main function
     * adds action to ActionList
     * @param boolean detailed - value for option to log detailed informations
     * @param int roundID
     * @param int duellID
     */
    public void simulate(boolean detailed, int roundID, int duellID)
    {
        setID(roundID, duellID);
        damageCalc.configureCalculator(knightList);
        
        while(knightList.getIDPlayer(1).getAlive() == true && knightList.getIDPlayer(2).getAlive() == true) {
            damageCalc.calc(detailed);

            // debug
            //System.out.println("Eine simulate() class(DuellSim) Ausführung !");
        }
        
        if(detailed == false) {
            knightActionList.addElement(new KnightDamageAction(knightList, detailed));
        }
        
        knightList.resetKnightNames();
        knightActionList.addElement(new KnightWinnerAction(knightList));
    }

    /**
     * setter for IDs roundID and duellID
     * iterates over knight objects and call theire setters for IDs
     * @param roundID
     * @param duellID
     */
    private void setID(int roundID, int duellID)
    {
        Iterator it = knightList.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            knight.setDuellID(duellID);
            knight.setRoundID(duellID);
        }
    }
}
